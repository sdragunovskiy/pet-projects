package dragunovskiy_sergey.linkedList;

public class IntLinkedList implements IntList, IntQueue, IntStack {
    private int size = 0;
    private Item first;
    private Item last;

    class Item {
        int value;
        Item next;
        Item prev;

        public Item(Item prev, int value, Item next) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }
    }

    @Override
    public void add(int value) {
        final Item l = last;
        final Item newNode = new Item(l,value,null);
        last = newNode;
        if (l == null) {
            first = newNode;
        }
        else {
            l.next = newNode;
        }
        size++;
    }



    @Override
    public boolean add(int index, int value) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("index less then zero");
        }
        if (index == 0) {
            addFirst(value);
        } else if(index == size - 1) {
            addLast(value);
        } else {
            Item newNode = new Item(null, value, null);
            Item currentNode = node(index);
            newNode.prev = currentNode.prev;
            newNode.next = currentNode.next;
            currentNode.prev.next = newNode;
            currentNode.next.prev = newNode;

        }
        if (node(index) == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void clear() {
        try {
            first = last = null;
            size = 0;
        } catch (NullPointerException e) {}
    }

    @Override
    public int get(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("index less than zero");
        }
        checkElementIndex(index);
        return node(index).value;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("index less than zero");
        }
        if (index == 0) {
            removeFirst(index);
        } else if (index == size - 1) {
            removeLast(index);
        } else {
            removeFromMiddle(index);
        }
        if (node(index) == null) {
            return false;
        } else {
            return true;
        }
    }

    private void removeFirst(int index) {
        Item firstNode = node(index);
        firstNode.next.prev = null;
        first = firstNode.next;
        size--;
    }

    private void removeFromMiddle(int index) {
        Item currentNode = node(index);
        currentNode.prev.next = currentNode.next;
        currentNode.next.prev = currentNode.prev;
        size--;
    }

    private void removeLast(int index) {
        Item lastNode = node(size - 1);
        lastNode.prev.next = null;
        last = lastNode.prev;
        size--;
    }

    @Override
    public boolean removeByValue(int value) {
        Item currentNode;
        for (int i = 0; i < size; i++) {
            currentNode = node(i);
            if (currentNode.value == value) {
                if (i == 0) {
                    removeFirst(i);
                } else if (i == size - 1) {
                    removeLast(i);
                } else {
                    removeFromMiddle(i);
                }
                return true;
            }
        }
        return false;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public IntList subList(int fromIndex, int toIndex) {
        IntLinkedList list = new IntLinkedList();
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(this.node(i).value);
        }
        return list;
    }

    @Override
    public int[] toArray() {
        int[] array = new int[this.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = this.get(i);
        }
        return array;
    }

    @Override
    public int remove() {
        int value = 0;
        Item firstNode = node(0);
        first = firstNode.next;
        value = firstNode.value;
        firstNode.next.prev = null;
        size--;
        return value;
    }

    @Override
    public int element() {
        int value = 0;
        Item firstNode = node(0);
        value = firstNode.value;
        return value;
    }

    @Override
    public int pop() {
        int value = 0;
        Item lastNode = node(size - 1);
        value = lastNode.value;
        lastNode.prev.next = null;
        last = lastNode.prev;
        size--;
        return value;
    }

    @Override
    public int peek() {
        int value = 0;
        Item lastNode = node(size - 1);
        value = lastNode.value;
        return value;
    }

    private Item node(int index) {
        if (index < (size >> 1)) {
            Item x = first;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
            return x;
        } else {
            Item x = last;
            for (int i = size - 1; i > index; i--) {
                x = x.prev;
            }
            return x;
        }
    }


    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void addFirst(int value) {
        Item newNode = new Item(null, value, null);
        Item currentNode = node(0);
        newNode.next = currentNode.next;
        currentNode.next.prev = newNode;
        first = newNode;
    }

    private void addLast(int value) {
        Item newNode = new Item(null, value, null);
        Item currentNode = node(size - 1);
        newNode.prev = currentNode.prev;
        currentNode.prev.next = newNode;
        last = newNode;
    }
}

