package dragunovskiy_sergey.linkedList;

public interface IntQueue {
    int remove();
    int element();
}
