package dragunovskiy_sergey.linkedList;

public interface IntStack {
    int pop();
    int peek();
}
