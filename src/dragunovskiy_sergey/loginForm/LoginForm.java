package dragunovskiy_sergey.loginForm;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LoginForm extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Login form");
        BorderPane firstRoot = new BorderPane();
        BorderPane secondRoot = new BorderPane();
        Button button = new Button("Login in");
        button.setStyle("-fx-background-color: #99e14b");
        VBox firstVBox = new VBox();
        VBox secondBox = new VBox();
        firstVBox.setAlignment(Pos.CENTER);
        firstVBox.setSpacing(10);
        firstVBox.setMaxWidth(250);
        firstRoot.setMaxWidth(150);
        secondBox.setAlignment(Pos.CENTER);

        Text loginText = new Text("Login");
        loginText.setFont(Font.font("Arial", 20));
        Text usernameText = new Text("Username:");
        Text passwordText = new Text("Password: ");
        Text messageText = new Text("");
        Text welcomeMessage = new Text("Welcome to app");
        welcomeMessage.setFont(Font.font("Arial", 35));

        HBox userName = new HBox();
        userName.setAlignment(Pos.BASELINE_LEFT);
        userName.setSpacing(20);
        userName.setPrefWidth(150);

        HBox password = new HBox();
        password.setAlignment(Pos.BASELINE_LEFT);
        password.setSpacing(20);
        password.setPrefWidth(150);

        HBox loginTextHBox = new HBox();
        loginTextHBox.getChildren().add(loginText);

        HBox buttonHBox = new HBox();
        buttonHBox.getChildren().add(button);
        buttonHBox.setAlignment(Pos.BASELINE_RIGHT);
        buttonHBox.setTranslateX(-13);

        HBox messageTextHBox = new HBox();
        messageTextHBox.getChildren().add(messageText);
        messageTextHBox.setTranslateX(80);

        TextField loginField = new TextField();
        loginField.setFont(Font.font("Arial", 13));
        PasswordField passwordField = new PasswordField();
        passwordField.setFont(Font.font("Arial", 13));

        userName.getChildren().addAll(usernameText, loginField);
        password.getChildren().addAll(passwordText, passwordField);

        firstVBox.getChildren().addAll(loginTextHBox, userName, password, messageTextHBox, buttonHBox);
        secondBox.getChildren().add(welcomeMessage);

        firstRoot.setCenter(firstVBox);
        secondRoot.setCenter(secondBox);
        Scene firstScene = new Scene(firstRoot, 400, 300, Color.OLIVEDRAB);
        Scene secondScene = new Scene(secondRoot, 400, 400, Color.OLIVEDRAB);

        stage.setScene(firstScene);

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try (BufferedReader bf = new BufferedReader(new FileReader("src/main/resources/login.txt"));) {
                    String line;
                    String valueLogin = "";
                    String valuePassword = "";
                    String[] array1;
                    String[] array2;
                    String[] array3;
                    while ((line = bf.readLine()) != null) {
                        array1 = line.split("\\|");
                        array2 = array1[0].split("=");
                        array3 = array1[1].split("=");
                        if (array2[1].equals(loginField.getText())) {
                            valueLogin = array2[1];
                            valuePassword = array3[1];
                        }
                    }
                    if (loginField.getText().equals("") && passwordField.getText().equals("")) {
                        messageText.setText("Incorrect login");
                    } else if (valueLogin.equals(loginField.getText()) && valuePassword.equals(passwordField.getText())) {
                        stage.setScene(secondScene);
                    } else {
                        messageText.setText("Incorrect login");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        stage.show();
    }
}

