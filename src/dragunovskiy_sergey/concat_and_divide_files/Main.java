package dragunovskiy_sergey.concat_and_divide_files;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args)  {
        // concatFiles("src/main/resources");
        // divideFile(outFile);
    }

    public static final String outFile = "path_to_file/out.txt";

    public static void concatFiles(String pathOfDirectory) {
        File fileForWrite = new File(outFile);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileForWrite))) {
            BufferedReader bufferedReader = null;
            File file = new File(pathOfDirectory);
            File[] listOfFiles = file.listFiles();
            for (File el : listOfFiles) {
                if (el.getName().endsWith(".txt") && !el.getPath().equals(outFile)) {
                    bufferedWriter.write(el.getName() + "\n");
                    bufferedReader = new BufferedReader(new FileReader(el.getAbsolutePath()));
                    String line = bufferedReader.readLine();
                    while (line != null) {
                        bufferedWriter.write(line + "\n");
                        line = bufferedReader.readLine();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void divideFile(String filePath) {
        filePath = outFile;
        List<String> list = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                list.add(line);
                line = bufferedReader.readLine();
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).indexOf(".txt") != -1) {
                    File newFile = new File("path/" + list.get(i) + "/");
                    BufferedWriter br = new BufferedWriter(new FileWriter(newFile));
                    for (int j = i + 1; j < list.size(); j++) {
                        if (list.get(j).indexOf(".txt") == -1) {
                            br.write(list.get(j) + "\n");
                        } else {
                            break;
                        }
                    }
                    br.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
