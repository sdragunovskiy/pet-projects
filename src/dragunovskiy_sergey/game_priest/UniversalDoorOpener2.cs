using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UniversalDoorOpener2 : MonoBehaviour
{
    public Camera _camera;
    private float distance = 3f;
    private Animator animator;
    private AudioSource soundOfDoor;
    private bool open;
    private string nameOfControllerBool;
    
   


    void Start()
    {
        animator = this.transform.GetComponent<Animator>();
        nameOfControllerBool = this.transform.gameObject.name;
        soundOfDoor = this.transform.GetComponent<AudioSource>();
      
        
    }

    
    void FixedUpdate()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance))
        {
            if (hit.transform.name == this.transform.name)
            {
                
                
                if (Input.GetKey(KeyCode.C))
                {
                    open = true;
                    animator.SetBool(nameOfControllerBool, open);
                    soundOfDoor.Play();

                }
                if (Input.GetKey(KeyCode.V))
                {
                    open = false;
                    animator.SetBool(nameOfControllerBool, open);
                    soundOfDoor.Play();
                }
            }
        }
    }
}
