using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeSomeObject : MonoBehaviour
{
    public GameObject camera;
    public float disnance = 15f;
    GameObject currentObject;
    bool canPickUp;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) PickUp();
        if (Input.GetKeyDown(KeyCode.G)) Drop();
       
    }

    void PickUp()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, disnance))
        {
            if (hit.transform.tag == "canPickUp")
            {
                if (canPickUp)
                {
                    Drop();
                }

                currentObject = hit.transform.gameObject;
                currentObject.transform.GetComponent<Rigidbody>().isKinematic = true;
                currentObject.transform.parent = transform;
                currentObject.transform.localPosition = Vector3.zero;
                canPickUp = true;
            }
        }
    }

    void Drop()
    {
        currentObject.transform.parent = null;
        currentObject.transform.GetComponent<Rigidbody>().isKinematic = false;
        canPickUp = false;
        currentObject = null;
    }
}
