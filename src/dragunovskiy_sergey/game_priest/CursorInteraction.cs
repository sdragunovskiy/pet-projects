using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorInteraction : MonoBehaviour
{
    public Camera _camera;
    public Image interactionCursor;
    private float distance = 3f;
    private GameObject revolver;
    private Image aim;

    void Start()
    {
        revolver = GameObject.Find("ReichsrevolverM1879");
        aim = revolver.transform.GetComponent<Aiming>().aim;
    }

    void Update()
    {
        interactionCursor.enabled = false;
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance))
        {
            if (hit.transform.tag == "canInteraction")
            {
                aim.enabled = false;
                interactionCursor.enabled = true;
            }
        }
    }
}
