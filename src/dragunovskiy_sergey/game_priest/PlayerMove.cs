using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private CharacterController characterController;
    private AudioSource soundOfSteps;

    public float speed = 7.0f;
    public float gravity = -9.8f;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        soundOfSteps = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartRun();
            soundOfSteps.pitch = soundOfSteps.pitch * 1.5f;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            FinishRun();
            soundOfSteps.pitch = soundOfSteps.pitch / 1.5f;
        }

        Vector3 movement = new Vector3(deltaX, 0, deltaZ);
        movement = Vector3.ClampMagnitude(movement, speed);

        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        movement.y = gravity;
        characterController.Move(movement);

        if (deltaX != 0.0f || deltaZ != 0.0f)
        {
            if (!soundOfSteps.isPlaying)
            {
                soundOfSteps.Play();

            }
        }
        else if (deltaX == 0.0f || deltaZ == 0.0f)
        {
            soundOfSteps.Pause();
        }

    }

    private void StartRun()
    {
        speed = speed * 2;
    }

    private void FinishRun()
    {
        speed = speed / 2;
    }
}
