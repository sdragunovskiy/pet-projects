using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Aiming : MonoBehaviour
{
    public Camera _camera;
    Coroutine zoom;
    public Image aim;

    public Animator animator;
    
    void Update()
    {
       if (Input.GetMouseButtonDown(1))
        {

            aim.enabled = false;
            animator.SetBool("aim", true);

            if (zoom != null)
            StopCoroutine(zoom);


            zoom = StartCoroutine(aimFieldOdView(_camera, 50, 0.3f));
            
        }
       if (Input.GetMouseButtonUp(1))
        {
            aim.enabled = true;
            animator.SetBool("aim", false);

            if (zoom != null)
            StopCoroutine(zoom);


           zoom = StartCoroutine(aimFieldOdView(_camera, 60, 0.3f));
        }
    }

    IEnumerator aimFieldOdView(Camera camera, float tofView, float duration)
    {
        float counter = 0;
        float fromView = camera.fieldOfView;


        while (counter < duration)
        {
            counter += Time.deltaTime;
            float viewTime = counter / duration;
            camera.fieldOfView = Mathf.Lerp(fromView, tofView, viewTime);
            yield return null;
        }
    }
}
