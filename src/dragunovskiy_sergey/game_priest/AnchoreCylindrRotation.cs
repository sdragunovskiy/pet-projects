using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchoreCylindrRotation : MonoBehaviour
{
    public Camera _camera;
    public float distance = 10f;
    public Animator animator;
    public bool canRotate;

    // Update is called once per frame
    void Update()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance))
        {
            if (hit.transform.tag == "anchore")
            {
                if (Input.GetKey(KeyCode.C)) RotateCylindr();
            }
        }
    }

    private void RotateCylindr()
    {
        Debug.Log("rotate");
        animator.SetBool("canRotate", canRotate);
        canRotate = true;
    }
}
