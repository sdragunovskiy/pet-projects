using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UniversalDrawerOpener2 : MonoBehaviour
{
    public Camera _camera;
    private float distance = 3f;
    private Animator animator;
    private AudioSource soundOfDrawer;
    private bool drawer;
    private string nameOfBool;
    private GameObject revolver;


    void Start()
    {
        animator = this.transform.GetComponent<Animator>();
        nameOfBool = this.transform.gameObject.name;
        soundOfDrawer = this.transform.GetComponent<AudioSource>();
    }

    
    void FixedUpdate()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance))
        {
            if (hit.transform.name == this.transform.name)
            {
                
                if (Input.GetKey(KeyCode.C))
                {
                    drawer = true;
                    animator.SetBool(nameOfBool, drawer);
                    soundOfDrawer.Play();
                }
                if (Input.GetKey(KeyCode.V))
                {
                    drawer = false;
                    animator.SetBool(nameOfBool, drawer);
                    soundOfDrawer.Play();
                }
            }
        }
    }
}
