3D-Game “Priest”
June 2022 - till now

The main character crashes in the middle of the ocean. Waking up on a raft, he notices a drifting ship. After inspecting the ship, the main character realizes that there is no one from the team on the ship. Soon the mystical reasons for their disappearance will be revealed.

Description:
Type: FPS game
Genre: horror, adventure
Levels: 4
Target Audience: 16+
Platform Affiliation: PC
OS: Windows/macOS

Stack of technologies: Unity, C#, GIT, Breadth-first search (BFS), Dijkstra's algorithm, Collections.
Programs: Unity, Visual Studio, Blender, Illustrator.

---------------------------------------------
There are only C# scripts