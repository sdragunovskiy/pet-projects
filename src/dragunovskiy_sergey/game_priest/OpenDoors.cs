using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoors : MonoBehaviour
{


    public Camera _camera;
    public float distance = 1f;
    public Animator animator;
    public bool isOpen;
    public AudioSource soundOfOpenDoor;
    public AudioSource soundOfCloseDoor;




    void Update()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance))
        {
            if (hit.transform.tag == "door")
            {
                if (Input.GetKey(KeyCode.C)) OpenShipDoor();
                if (Input.GetKey(KeyCode.V)) CloseShipDoor();
            }
        }

       
    }

    private void OpenShipDoor()
    {
        animator.SetBool("isOpen", isOpen);
        isOpen = true;
        soundOfOpenDoor.Play();
       
    }

    private void CloseShipDoor()
    {
        animator.SetBool("isOpen", isOpen);
        isOpen = false;
        soundOfCloseDoor.Play();
    }
}
