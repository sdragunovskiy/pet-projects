using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraAim : MonoBehaviour
{
    private Camera camera;
    Animation animation;

    public Texture2D cursorNormal;
    public Texture2D cursorTake;
    private Texture2D cursor;

    private GameObject door;
    private BoxCollider doorCollider;


    void Start()
    {
        camera = GetComponent<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }


    void MainCursor(string tag)
    {
        if (tag == "key" || tag == "canTake" || tag == "door")
        {
            cursor = cursorTake;
        }
        else
        {
            cursor = cursorNormal;
        }
    }

    private void FixedUpdate()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            MainCursor(hit.transform.tag);
        }
        else
        {
            cursor = cursorNormal;
        }




        //if (Input.GetMouseButton(0))
        //{

        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        BoxCollider hitCollider = hit.collider.GetComponent<BoxCollider>();



        //        if (hitCollider == doorCollider)
        //        {

        //            door.transform.GetComponent<OpenDoor>().Open();
        //        } 

        //    }

        //}


    }

    private void OnGUI()
    {
        int size = 40;
        float posX = camera.pixelWidth / 2 - size / 4;
        float posY = camera.pixelHeight / 2 - size / 2;

        {
            GUI.Label(new Rect(posX, posY, size, size), cursor);
        }


    }
}
