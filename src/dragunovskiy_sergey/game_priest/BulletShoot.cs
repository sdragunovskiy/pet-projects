using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShoot : MonoBehaviour
{
    public GameObject bullet;
    public Camera _camera;
    public Transform bulletSpawn;
    public AudioSource soundOfShot;
    public Animator animator;
    public bool isShot = false;
   
     


    public float shootForce;
    public float spread;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();

        }

        if (Input.GetMouseButtonUp(0))
        {
            isShot = false;
            animator.SetBool("shot", isShot);
        }


       
    }

    private void Shoot()
    {
        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        Vector3 targerPoint;

        if (Physics.Raycast(ray, out hit))
        {
            targerPoint = hit.point;
        } else
        {
            targerPoint = ray.GetPoint(75);
        }

        Vector3 distanceWithoutSpread = targerPoint - bulletSpawn.position;
        float spreadForX = Random.Range(-spread, spread);
        float spreadForY = Random.Range(-spread, spread);

        Vector3 distanceWithSpread = distanceWithoutSpread + new Vector3(spreadForX, spreadForY, 0);

        GameObject currentBullet = Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
        currentBullet.transform.forward = distanceWithSpread.normalized;

        currentBullet.GetComponent<Rigidbody>().AddForce(distanceWithSpread.normalized * shootForce, ForceMode.Impulse);

        isShot = true;
        animator.SetBool("shot", isShot);
        soundOfShot.Play();
        

    }
}
