-- create new table roles
create table car_rent.roles
(
    id   int
        constraint roles_pk
            primary key,
    role varchar(50)
);

-- add roles into table roles
update car_rent.roles set role = 'Admin'
where id = 1;
update car_rent.roles set role = 'Manager'
where id = 2;
update car_rent.roles set role = 'Client'
where id = 3;


-- create new table managers
create table car_rent.managers
(
    id       int
        constraint managers_pk
            primary key,
    name     varchar(40),
    login    varchar(40),
    password varchar(40),
    email    varchar(50),
    role     int
        constraint managers_roles_id_fk
            references car_rent.roles
);

-- add new column manager_id to table orders
alter table car_rent.orders
    add manager_id int;


-- add foreign key
alter table car_rent.orders
    add constraint orders_managers_id_fk
        foreign key (manager_id) references car_rent.managers;

-- add new records to car_rent.cars
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (6, 'Toyota', 'Camry', 2020, 55);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (7, 'Suzuki', 'Vitara', 2019, 53);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (8, 'BMW', 'X8', 2020, 70);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (9, 'Cadillac', 'Escalade', 2019, 75);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (10, 'Land Rover', 'Discovery', 2019, 70);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (11, 'Range Rover', 'Velar', 2020, 57);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (12, 'Opel', 'Corsa', 2018, 45);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (13, 'Opel', 'Zafira', 2020, 50);
insert into car_rent.cars (id, manufacturer, model, year, price) VALUES (14, 'Lexus', 'RX', 2020, 70);

-- add new records to car_rent.managers
insert into car_rent.managers (id, name, login, password, email, role) VALUES (1, 'Dmitry', 'Dimas', 1111, 'dimas@gmai.com', 1);
insert into car_rent.managers (id, name, login, password, email, role) VALUES (2, 'Max', 'Maxx', 2222, 'max@gmai.com', 2);
insert into car_rent.managers (id, name, login, password, email, role) VALUES (3, 'Ivan', 'Ivannn', 3333, 'ivan@gmai.com', 1);
insert into car_rent.managers (id, name, login, password, email, role) VALUES (4, 'Petr', 'Petrrr', 4444, 'petr@gmai.com', 2);
insert into car_rent.managers (id, name, login, password, email, role) VALUES (5, 'Alexey', 'Alex', 5555, 'alex@gmai.com', 1);

-- add new records to car_rent.clients
insert into car_rent.clients (id, name, surname, phone) VALUES (4, 'Daniil', 'Danilov', 38056775775);
insert into car_rent.clients (id, name, surname, phone) VALUES (5, 'Andrey', 'Andreev', 3805675665);
insert into car_rent.clients (id, name, surname, phone) VALUES (6, 'Anna', 'Ivaniva', 38057878875);
insert into car_rent.clients (id, name, surname, phone) VALUES (7, 'Denis', 'Denisov', 38050555666);
insert into car_rent.clients (id, name, surname, phone) VALUES (8, 'Alexandr', 'Alexandrov', 380669998887);

-- add new column in car_rent.cars
alter table car_rent.cars
    add available boolean;



-- select all managers witch has role admin
select * from car_rent.managers
where role = 1;

-- select all records from orders where date > then 1.28.2022
select * from car_rent.orders
where date(date) > '2022-01-28';

-- select max manufacture and model of cars where price = max price
select manufacturer,  model from car_rent.cars
where price = (select max(price) from car_rent.cars);

-- select count of managers witch has role manager
select count(car_rent.managers.id) from car_rent.managers where car_rent.managers.role = 2;

-- select all cars witch has records in orders
select manufacturer, model from car_rent.cars
where car_rent.cars.id in (select car_id from car_rent.orders);

-- select count of manufacturer of cars
select count(manufacturer) as count, manufacturer from car_rent.cars
group by manufacturer;

-- create transaction
begin;
insert into car_rent.orders (id, date, car_id, client_id, manager_id)
values (11, '2022-01-11', 2, 3, 2),
       (12, '2019-03-22',3, 2, 3);
update car_rent.cars set available = false
where car_rent.cars.id in (select car_id from car_rent.orders);
commit;
