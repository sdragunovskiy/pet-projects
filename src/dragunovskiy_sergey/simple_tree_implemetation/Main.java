package dragunovskiy_sergey.simple_tree_implemetation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        countDuplicates(40);

        Node root = new Node("One");

        Node two = new Node("Two");
        Node three = new Node("three");

        Node four = new Node("four");
        Node five = new Node("five");

        Node six = new Node("six");
        Node seven = new Node("seven");

        Node eight = new Node("eight");

        root.addChild(two);
        root.addChild(three);

        two.addChild(four);
        two.addChild(five);

        three.addChild(six);
        three.addChild(seven);
        three.addChild(eight);


        root.insert(new Node("newNode"), "five");
        root.printTree();

    }

    public static void countDuplicates(int size) {
        List<Integer> list = new ArrayList<>();
        Map<Integer, Integer> mapDuplicate = new HashMap<>();
        for (int i = 0; i <= size; i++) {
            int random = (int) (1 + Math.random() * 100);
            list.add(random);
        }

        for (int i = 0; i < list.size(); i++) {
            Integer number = list.get(i);
            if (!mapDuplicate.containsKey(number)) {
                mapDuplicate.put(number, 1);
            } else {
                mapDuplicate.put(number, mapDuplicate.get(number) + 1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : mapDuplicate.entrySet()) {
            if (entry.getValue() > 1) {
                System.out.println("Number: " + entry.getKey() + " count: " + entry.getValue());
            }
        }
    }
}
