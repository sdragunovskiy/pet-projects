package dragunovskiy_sergey.simple_tree_implemetation;

import java.util.*;

public class Node {
    private final String name;
    private List<Node> children;

    public Node(String name) {
        this.name = name;
        this.children = new ArrayList<>();

    }

    public String getName() {
        return name;
    }

    private void getSearchingChildren() {
        for (int i = 0; i < children.size(); i++) {
            System.out.print(children.get(i));
            System.out.println("");
        }
    }

    public List<Node> getChildren() {
        return children;
    }

    public void addChild(Node children) {
        this.children.add(children);
    }

    public void printTree() {
        System.out.println(this.getName());
        System.out.println("|");
        if (children != null) {
            this.getSearchingChildren();
        }
    }

    public void insert(Node newNode, String parentName) {
        Deque<Node> search = new LinkedList<>();
        for (int i = 0; i < this.getChildren().size(); i++) {
            search.addFirst(this.getChildren().get(i));
        }
        if (this.getName().equals(parentName)) {
            this.addChild(newNode);
        } else {
            while (!search.isEmpty()) {
                Node node = search.poll();
                if (node.getName().equals(parentName)) {
                    node.addChild(newNode);
                } else {
                    for (int i = 0; i < node.getChildren().size(); i++) {
                        search.addFirst(node.getChildren().get(i));
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return name +
                " --- " + children;
    }
}

