package dragunovskiy_sergey.gson_parser.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Cars")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cars {
    @XmlElement(name = "Car")
    private List<Car> carList = null;

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }
}
