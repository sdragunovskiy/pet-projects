package dragunovskiy_sergey.gson_parser.objects;

import lombok.*;
import dragunovskiy_sergey.gson_parser.DateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlRootElement(name = "Car")
@XmlAccessorType(XmlAccessType.FIELD)
public class Car {
    private String name;
    private int milesPerGallon;
    private int cylinders;
    private int displacement;
    private int horsePower;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date year;
    private String origin;
    private Location location;


}
