package dragunovskiy_sergey.gson_parser.objects;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Location {
    private String country;
    private String city;
    private String street;
    private int house;

}

