package dragunovskiy_sergey.gson_parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import dragunovskiy_sergey.gson_parser.objects.Car;
import dragunovskiy_sergey.gson_parser.objects.Location;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GSonParser {
    public List<Car> parseCarObjects() throws java.text.ParseException {
        List<Car> cars = new ArrayList<>();
        JSONParser parser = new JSONParser();
        try (FileReader reader = new FileReader("src/main/resources/cars.json")) {
            JSONArray jsonArray = (JSONArray) parser.parse(reader);
            for (Object el : jsonArray) {
                JSONObject objectCar = (JSONObject) el;
                Car newCar = new Car();
                if (objectCar.get("location") == null) {
                    setValueFromJson(objectCar, newCar, cars);
                } else if (objectCar.get("location") != null) {
                    JSONObject objectLocation = (JSONObject) ((JSONObject) el).get("location");
                    setValueFromJsonWithLocation(objectCar, newCar, cars, objectLocation);
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return cars;
    }

    private void setValueFromJson(JSONObject objectCar, Car newCar, List<Car> cars) throws java.text.ParseException {
        String carName = (String) objectCar.get("name");
        long carMilesPerGallon;
        if (objectCar.get("miles_per_Gallon") != null) {
            carMilesPerGallon = (long) objectCar.get("miles_per_Gallon");
            newCar.setMilesPerGallon((int) carMilesPerGallon);
        }
        long carCylinders = (long) objectCar.get("cylinders");
        long carDisplacement = (long) objectCar.get("displacement");
        long carHorsePower = (long) objectCar.get("horsepower");
        String date = (String) objectCar.get("year");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date carYear = simpleDateFormat.parse(date);
        String carOrigin = (String) objectCar.get("origin");
        newCar.setName(carName);
        newCar.setCylinders((int) carCylinders);
        newCar.setDisplacement((int) carDisplacement);
        newCar.setHorsePower((int) carHorsePower);
        newCar.setYear(carYear);
        newCar.setOrigin(carOrigin);
        cars.add(newCar);
    }

    private void setValueFromJsonWithLocation(JSONObject objectCar, Car newCar, List<Car> cars, JSONObject objectLocation) throws java.text.ParseException {
        String locationCountry = (String) objectLocation.get("country");
        String locationCity = (String) objectLocation.get("city");
        String locationStreet = (String) objectLocation.get("street");
        long locationHouse = (long) objectLocation.get("house");
        Location newLocation = new Location();
        newLocation.setCountry(locationCountry);
        newLocation.setCity(locationCity);
        newLocation.setStreet(locationStreet);
        newLocation.setHouse((int) locationHouse);

        String carName = (String) objectCar.get("name");
        if (objectCar.get("miles_per_Gallon") != null) {
            long carMilesPerGallon;
            carMilesPerGallon = (long) objectCar.get("miles_per_Gallon");
            newCar.setMilesPerGallon((int) carMilesPerGallon);
        }
        long carCylinders = (long) objectCar.get("cylinders");
        long carDisplacement = (long) objectCar.get("displacement");
        long carHorsePower = (long) objectCar.get("horsepower");
        String date = (String) objectCar.get("year");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date carYear = simpleDateFormat.parse(date);
        String carOrigin = (String) objectCar.get("origin");

        newCar.setName(carName);
        newCar.setCylinders((int) carCylinders);
        newCar.setDisplacement((int) carDisplacement);
        newCar.setHorsePower((int) carHorsePower);
        newCar.setYear(carYear);
        newCar.setOrigin(carOrigin);
        newCar.setLocation(newLocation);
        cars.add(newCar);
    }
}

