package dragunovskiy_sergey.gson_parser;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAdapter extends XmlAdapter<String, Date> {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date unmarshal(String s) throws Exception {
        return formatter.parse(s);
    }

    @Override
    public String marshal(Date date) throws Exception {
        return formatter.format(date);
    }
}
