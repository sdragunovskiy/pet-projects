package dragunovskiy_sergey.gson_parser;

import dragunovskiy_sergey.gson_parser.objects.Car;
import dragunovskiy_sergey.gson_parser.objects.Cars;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws JAXBException, ParseException, ParseException {
        String filePath = "path_to_file";
        GSonParser gSonParser = new GSonParser();
        List<Car> carsList = gSonParser.parseCarObjects();
        convertObjectToXML(carsList, filePath);
    }

    public static void convertObjectToXML(List<Car> list, String filePath) throws JAXBException {
        Cars cars = new Cars();
        cars.setCarList(new ArrayList<Car>());
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLocation() != null) {
                cars.getCarList().add(list.get(i));
            }
        }
        File file = new File(filePath);
        JAXBContext context = JAXBContext.newInstance(Cars.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(cars, file);
    }

}
